package com.zuitt.wdc044;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

// "@SpringBootApplication" is an example of "Annotations" mark.
// Annotations are used to provide supplemental information about the program.
// These are used to manage and configure the behavior of the framework.
// Annotations are used extensively in Spring Boot to configure components, define dependencies, or enable specific functionalities
// Spring Boot scans for classes in its class path upon running and assigns behaviors on them based on their annotations.

//This annotation specifies the main class of the Spring Boot application
@SpringBootApplication

//This indicates that the class is a controller that will handel RESTful web request and returns a HTTP response.
@RestController
public class Wdc044Application {

    public static void main(String[] args) {
        //		This method starts the whole Spring framework
        //		This serves as the entry point to start the application
        SpringApplication.run(Wdc044Application.class, args);
    }

//    This is used for mapping HTTP GET Requests.
//    @GetMapping annotation is always followed by a method body
//    Method body contains the logic to generate the response
    @GetMapping("/hello")
//    @RequestParam() is used to extract query parameters, form parameters followed by "key=value" pair.
//        "?" means the start of parameters followed by key=value pairs

//    Example : localhost:8080/hello?name=jeleel
    public String hello(@RequestParam(value = "name", defaultValue = "World")String name) {
//        return "Hello " + name;
        return String.format("Hello %s", name);
    }

    @GetMapping("/hi")
    public String hi(@RequestParam(value = "user", defaultValue = "user") String user) {
        return String.format("hi %s!", user);
    }

    @GetMapping("/nameAge")
    public String nameAge(@RequestParam(value = "name", defaultValue="user") String name,
                          @RequestParam(value= "age", defaultValue="0") int age) {
        return String.format("Hello %s! Your age is %d.", name, age);
    }
}
